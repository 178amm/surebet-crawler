/*
 * Module dependencies
 */
var mongoose = require('mongoose');
mongoose.Promise = Promise; 
var Schema = mongoose.Schema;
var logger = require('logger');
var private = {};
var diac = require('../../helpers/diacritics');
var moment = require('moment');

const MIN_SCORE = 12;


/**
 * User schema
 */

var MatchSchema = new Schema({

  player1: { type: String, required: true },
  player2: { type: String, required: true },
  league: { type: String },
  date: { type: Date, required: true },
  sport: { type: String, required: true },
  odds: [{ 
    url: { type: String },
    bookmaker: { type: String, required: true }, // Unique per odd
    odd1: { type: Number, required: true },
    oddt: { type: Number },
    odd2: { type: Number, required: true },
    createdAt: { type: Date, default: Date.now }
  }],
  searcheable: { type: String, required: true },

});

// MatchSchema.virtual('searcheable').get(function () {
//   return this.player1 + ' ' + this.player2;
// });


/**
 * Text indexes
 */

MatchSchema.index({
    'searcheable': "text",
}, {
    weights: {
      'searcheable': 10,
    },
    name: "searchIndex"
});

/**
 * Schema pre's & post
 */

MatchSchema.pre('save', function(next) {
  
    //Parse searcheable field
    this.searcheable = generateSearcheable(this);
    // Generate coef
    //this.surebet_coef = generateSurebetCoef(this);

    next();
});

/**
 * User plugin
 */

//UserSchema.plugin(userPlugin, {});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

 //@TODO block editing pass

/**
 * Methods
 */

MatchSchema.method({

  upsertOdds: function (oddCrawl, cb){
    //Setup odds
    var odd = oddCrawl.odds;
    //Tries to find the oddset by bookmaker's id
    var f = null;
    var i = 0;
    for(i; i<this.odds.length && !f; i++){
      if (this.odds[i].bookmaker == odd.bookmaker)
        f = true;
    }
    i--;

    //If found, remove, were are going to update it
    if (f)
      this.odds.splice(i, 1);

    //Appends the odd
    this.odds.push(odd);
    //logger.debug('After appending', this);
    
    //Saves
    this.save()
    .then(data => {
      cb(null, {doc: data, upserted: f});
    })
    .catch(err => {
      cb(err);
    });

    //cb(null, this);
  },

  // upsertOdds: function (oddCraw, cb){

  //   var bulk = Model.collection.initializeOrderedBulkOp();

  //   bulk.find({ "_id": id, "readers.user": req.user.id }).updateOne({
  //       "$set": { "readers.$.someData": data } 
  //   });

  //   bulk.find({ "_id": id, "readers.user": { "$ne": req.user.id } }).updateOne({
  //       "$push": { "readers": { "user": req.user.id, "someData": data } }
  //   });

  //   bulk.execute(function(err,result) {
  //       // deal with result here
  //   });

  // }

});

/**
 * Statics
 */

MatchSchema.static({

  // updateOdds: function (data, cb){ //(oddCrawl, cb)

  //   //@TODO parse data
  //   var upsertion = { 
  //     $push: { 
  //       odds: { 
  //         bookmaker: data.odds[0].bookmaker, 
  //         odd1: data.odds[0].odd1,
  //         odd2: data.odds[0].odd2,
  //         oddt: data.odds[0].oddt
  //       }
  //   }
  // };

  //   logger.info(upsertion);

  //   var query = { date: data.date, "odds.bookmaker": data.bookmaker, $text : { $search : data.searcheable } };
  //   var opts = { score : { $meta: "textScore" }, sort: { score: -1 } }

  //   //@TODO filter by min score

  //   this.findOneAndUpdate(query, upsertion, opts)
  //     .exec(cb);
  // },

  searchMatch: function (data, cb){

    // var top_date = data.date.setDate(data.date.getDate()+1);
    // var bottom_date = data.date.setDate(data.date.getDate()-1);
    var top_date = moment(data.date).add(1, 'days');
    var bottom_date = moment(data.date).subtract(1, 'days');
    // logger.debug('Top date', new Date(top_date))
    // logger.debug('Bottom date',new Date(bottom_date))
    var query = { date: { $gt: bottom_date, $lt: top_date }, sport: data.sport, $text : { $search : data.searcheable } };
    //var query = { date: data.date, $text : { $search : data.searcheable } };
    var opts = { score : { $meta: "textScore" } };
    var sort = { score : { $meta: "textScore" } };

    this.findOne(query, opts)
      .sort(sort)
      .exec(function(err, data){
        //@WATCHOUT - Filter by score
        if(data && data._doc.score>MIN_SCORE){
          cb(err, data);
        }
        else
          cb(err, null);
      });
  },


});

/**
 * Register
 */

mongoose.model('Match', MatchSchema);


/**
 * Privates
 */

function generateSearcheable(data){
  return diac.removeDiacritics((data.player1+' '+data.player2).toLowerCase());
}

function generateSurebetCoef(data){
    //Recalc surebet
    var surebet_coef = null;
    // var modd1 = Math.max.apply(Math, data.odds.map(function(o){return o.odd1;}));
    // var modd2 = Math.max.apply(Math, data.odds.map(function(o){return o.odd2;}));

    if (data.odds.length>1){
      logger.debug('Calculating coef for', data.player1 + ' ' + data.player2);
      // Gets index of max odd
      var map1 = data.odds.map(function(o){return o.odd1;});
      var m1 = map1.indexOf(Math.max(...map1));
      // Delete found bookmaker since both shouldt match
      data.odds.slice(m1, 1);
      // Re-search
      var map2 = data.odds.map(function(o){return o.odd2;});
      var m2 = map2.indexOf(Math.max(...map2));

      logger.debug('Ms', m1 + ', ' + m2);

      //@TODO, Should get a different house
      surebet_coef = (1-(1/data.odds[m1].odd1 + 1/data.odds[m2].odd2 ))*100;

      logger.debug('Coef', surebet_coef);
    }
    
    return surebet_coef;
}