/*
 * Module dependencies
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var logger = require('logger');
var private = {};
const MIN_SCORE = 18;

/**
 * User schema
 */

var ProductSchema = new Schema({
  brand: { type: String, required: true },
  product: { type: String, required: true },
  color: { type: String },
  size: { type: String },
  detail: { type: String },
  image_url: { type: String },
  links: [{ //Links to different stores
    product_uuid: { type: String }, //, unique: true 
    store_id: { type: String },
    url: { type: String }, 
  }]
});

/**
 * Text indexes
 */

ProductSchema.index({
    'product': "text",
    'color': "text",
    'size': "text",
    'detail': "text",
}, {
    weights: {
      'product': 10,
      'color': 8,
      'size': 2,
      'detail': 4
    },
    name: "searchIndex"
});

/**
 * Schema pre's & post
 */

ProductSchema.pre('save', function(next) {

    //Parse brand field
    this.brand = this.brand.toLowerCase().trim();

    next();
});

/**
 * User plugin
 */

//UserSchema.plugin(userPlugin, {});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

 //@TODO block editing pass

/**
 * Methods
 */

ProductSchema.method({

  addLinks : function (data, cb){
    //@TODO parse link structure
    this.links.push({store_id: data.shop, url: data.url});
    this.save(cb)
    //cb(null);
  },

});

/**
 * Statics
 */

ProductSchema.static({

  searchSimilarOld : function (data, cb){
    //@TOCHANGE This is momentary
    this.findOne({color: data.color, product: data.product})  
      .exec(cb);
  },

  searchSimilar : function (data, cb){
    //var MIN_SCORE = 15;
    var textsearch = data.product+' '+(data.detail||'')+' '+(data.color||''); 

    logger.debug('PRODUCT-M: Findin similar for ', data);
    logger.debug('PRODUCT-M: Textsearch: ', textsearch);

    this.find( 
        { brand: data.brand.toLowerCase(),  
          "links.store_id": { $nin: data.shop},
          //score: {$gte: MIN_SCORE} },
          $text : { $search : textsearch } }, // Search by textsearch query
        { score : { $meta: "textScore" } } // Retrieve score & filters the best
    )
    .sort({ score : { $meta : 'textScore' } })
    .exec(function(err, returned){
      logger.debug('PRODUCT FOUND!', returned);
      // Last filter, exclude if doesnt pass min score
      if (returned && returned._doc && returned._doc.score < MIN_SCORE)
        returned = null;
      else
        returned = returned.length ? returned[0] : null;

      cb(err, returned);
    });
  },

  searchSimilarAndUpdateLinks : function(data, cb){
    var self = this;
    //Search unique similar
    private.searchUniqueSimilar(self, data, function(err, similar){
      if (err)
        cb(err)
      else if (!similar)
        cb()
      else{
        var query = {_id: similar._id, "links.store_id": { $nin: data.shop}};
        var update = {$push: {links: {store_id: data.shop, url: data.url, product_uuid: data.product_uuid}}};
        self.findOneAndUpdate(query, update, cb);
      }
    });
  }

  // searchSimilarAndUpdateLinks : function(data, cb){
    
  //   var textsearch = data.product+' '+(data.detail||'')+' '+(data.color||''); 

  //   logger.debug('PRODUCT-M: Findin similar for ', data);
  //   logger.debug('PRODUCT-M: Textsearch: ', textsearch);

  //   var query = { brand: data.brand.toLowerCase(),  
  //         "links.store_id": { $nin: data.shop }, // Exclude same store products
  //         //"links.url": { $nin: data.url },
  //         $text : { $search : textsearch }, // Search by textsearch query
          
  //       };  
  //   var update = {$push: {links: {store_id: data.shop, url: data.url}}};
  //   var match = { $match: { score: { $gte : MIN_SCORE } } };

  //   this.findOneAndUpdate(query, update)
  //   .select({ score: { "$meta": "textScore" } })
  //   .sort({ score: '-1' })
  //   //.where('score')
  //   //.elemMatch(match)
  //   .exec(function(err, returned){
  //     logger.debug('PRODUCT FOUND!', returned);
  //     // Last filter, exclude if doesnt pass min score
  //     // if (returned._doc && returned._doc.score < MIN_SCORE)
  //     //   returned = null;
  //     // else
  //     //   returned = returned.length ? returned[0] : null;

  //     cb(err, returned);
  //   });
  // }

  // search : function (params, cb) {
  //   var query = {};
  //   var by = params.by;
  //   var text = params.text;

  //   //Add regexp to the query
  //   if (text && text.length)
  //     query[by] = new RegExp(text, 'i');

  //   this.find(query)
  //       .exec(cb);
  // }, 

  // update: function (document, cb){

  // }
});

/**
 * Register
 */

mongoose.model('Product', ProductSchema);


/**
 * Privates
 */

private.searchUniqueSimilar = function (schema, data, cb){

  //@TODO should validate fields
  var textsearch = data.product+' '+(data.detail||'')+' '+(data.color||''); 

  logger.debug('PRODUCT-M: Findin similar for ', data);
  logger.debug('PRODUCT-M: Textsearch: ', textsearch);

  schema.find( 
        { brand: data.brand.toLowerCase(),  
          "links.store_id": { $nin: data.shop},
          $text : { $search : textsearch } }, // Search by textsearch query
        { score : { $meta: "textScore" } } // Retrieve score & filters the best
    )
    .select({ score: { $meta: "textScore" } })
    .sort({ score : { $meta : 'textScore' } })
    .exec(function(err, similars){
      if (err)
        cb(err);
      else{
        logger.debug('PRODUCT-M: Product list found:', similars);
        var unique = null;
        //Data found
        if (similars.length){
          logger.debug('PRODUCT-M: Passes length');
          //First should pass the minimal score
          if (similars[0]._doc.score>MIN_SCORE){
            logger.debug('PRODUCT-M: Passes min score');
            //Second, score should be different from the second one
            if(similars.length>1 && similars[0]._doc.score>similars[1]._doc.score){
              logger.debug('PRODUCT-M: Passes greater than second element');
              unique = similars[0]
            }
            //Or, there is just one element
            else if (similars.length==1){
              logger.debug('PRODUCT-M: Passes only one element');
              unique = similars[0]
            }
          }
        }
        //Return
        logger.debug('PRODUCT-M: Unique found:', unique);
        cb(null, unique);
      }
    });
}
