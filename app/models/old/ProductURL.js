/**
 * Requires
 */
const redis = require('../../node_modules_custom/redis-singleton-connection');

/**
 * Constructor
 */
var ProductURL = function(config) {
  // Vars
  this.config = config;
  this.TABLE_NAME = 'ProductURLSet';
}

/**
 * Properties
 */
ProductURL.prototype = {  

  //Adds a new PageURL
  add: function(url_s, store, cb){
    client = redis.getClient();
    if (url_s)
      client.sadd(this.TABLE_NAME+":"+store, url_s, cb);
    else
      cb(new Error('REDIS: Error, undefined producturl'));
  },

  getAll: function(store, cb){
    client = redis.getClient();
    client.smembers(this.TABLE_NAME+":"+store, cb);      
  }
};

/**
 * Exports
 */
module.exports = new ProductURL();


/**
 * Privates
 */