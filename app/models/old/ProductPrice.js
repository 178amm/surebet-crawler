/**
 * Requires
 */
const redis = require('../../node_modules_custom/redis-singleton-connection');
const moment = require('moment');
const md5 = require('md5');
const logger = require('logger');

/**
 * Constructor
 */
var ProductPrice = function(config) {
  // Vars
  this.config = config;
  this.TABLE_NAME = 'ProductPriceHash';
  this.RELATION_NAME = 'ProductPriceKeys';
}

/**
 * Properties
 */
ProductPrice.prototype = {  

  //Adds a new PageURL
  add: function(data, cb){
    client = redis.getClient();
    if (data){
      // Set price update & save
      var toset = buildObject(this.TABLE_NAME, data)
      client.hmset(toset, function(err, data){
        if (err)
          cb(err);
        else if (!data)
          cb(null, null);
        else
          cb(null, toset);
      });
    }
    else
      cb(new Error('REDIS: Error, undefined product data'));
  },

  get: function(product_uuid, cb){
    client = redis.getClient();
    client.hgetall(this.TABLE_NAME+":"+product_uuid, cb);
  },

  setProductUUID: function(product_uuid, product_store_id, cb){
    client = redis.getClient();
    if(product_uuid && product_store_id)
      client.set(this.RELATION_NAME+":"+product_store_id, product_uuid.toString(), cb);
    else
      cb(new Error('REDIS: Error, undefined uuid or store_id'));
  },

  getProductUUID: function(product_store_id, cb){
    client = redis.getClient();
    if(product_store_id)
      client.get(this.RELATION_NAME+":"+product_store_id, cb)
    else
      cb(new Error('REDIS: No product_store_id supplied'));
  },

  // Tiene que ser el mas nuevo y el mas barato
  // @TOWATCH , not sure if it will work
  checkIfLatestIsTheCheapest: function(hash_keys){
      //Setup arrays 
      var tarr = Object.keys(hash_keys).map(function (key, index, arr) { return parseInt(key.split(':')[1]); });
      var larr = Object.keys(hash_keys).map(function ( key ) { return hash_keys[key]; });

      //Get the latest added (index)
      var max = tarr.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);
      //Get the lowest price (index)
      var min = larr.reduce((iMax, x, i, arr) => x < arr[iMax] ? i : iMax, 0); //Sticks to the latest in time, so it will work

      // logger.debug('PRODUCTPRICE: LAST ADDED BY INDEX: ' + max, tarr[max]);
      // logger.debug('PRODUCTPRICE: CHEAPEST BY INDEX: ' + min, larr[min]);

      if (max == min)
          return true //Two indexes matches, latest added is the cheapest
      else
          return false

      // Try this:

      // var arr = Object.keys( obj ).map(function ( key ) { return obj[key]; });
      // and then:

      // var min = Math.min.apply( null, arr );
      // var max = Math.max.apply( null, arr );
      // Live demo: http://jsfiddle.net/7GCu7/1/

      // Update: Modern version (ES6+)

      // let obj = { a: 4, b: 0.5 , c: 0.35, d: 5 };

      // let arr = Object.values(obj);
      // let min = Math.min(...arr);
      // let max = Math.max(...arr);

      // console.log( 'Min value: ' + min + ', max value: ' + max );
  },


};

/**
 * Exports
 */
module.exports = new ProductPrice();


/**
 * Privates
 */
function buildObject(tablename, data){ // req {store_id, product_id, price}
  var object = [];

  object.push(tablename+":"+data.product_uuid.toString());
  object.push(data.shop+":"+moment().unix(), data.price);  

  return object;
}