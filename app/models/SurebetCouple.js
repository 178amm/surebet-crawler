/*
 * Module dependencies
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var logger = require('logger');

/**
 * User schema
 */

var SurebetSchema = new Schema({
  odd1: { 
    url: { type: String },
    bookmaker: { type: String, required: true },
    odd1: { type: Number, required: true }
  },
  odd2: { 
    url: { type: String },
    bookmaker: { type: String, required: true },
    odd2: { type: Number, required: true }
  },
  oddt: { 
    url: { type: String },
    bookmaker: { type: String },
    odd: { type: Number }
  },
  profitability : { type: Number, required: true },
  match_data: { type: Schema.Types.Mixed, required: true },
  createdAt: { type: Date, default: Date.now }
});

/**
 * Text indexes
 */


/**
 * Schema pre's & post
 */

// SurebetSchema.pre('save', function(next) {

//     var self = this;

//     var odds = JSON.parse(JSON.stringify(self.match_data.odds));

//     // Gets index of max odd
//     var map1 = odds.map(function(o){return o.odd1;});
//     var m1 = map1.indexOf(Math.max(...map1));
//     var d1 = odds[m1];
//     // Delete found bookmaker since both shouldt match
//     odds.splice(m1, 1);
//     // Re-search
//     var map2 = odds.map(function(o){return o.odd2;});
//     var m2 = map2.indexOf(Math.max(...map2));
//     var d2 = odds[m2];

//     self.odd1.odd = d1.odd1;
//     self.odd1.bookmaker = d1.bookmaker;
//     self.odd2.odd = d2.odd2;
//     self.odd2.bookmaker = d2.bookmaker;

//     next();
// });

/**
 * User plugin
 */

//UserSchema.plugin(userPlugin, {});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

 //@TODO block editing pass

/**
 * Methods
 */

SurebetSchema.method({

  toHTML : function (params){

    var message = "";

    message += '<b>' + this.match_data.sport.capitalize() + '</b> ' + parseSport(this.match_data.sport) + '\n';
    message += 'Rentabilidad de ' + '<b>' + parseProfit(this.profitability, params) + '</b>' + '\n';
    message += this.match_data.player1 + ' @ <b>' + this.odd1.odd1 + '</b> en ' + '<a href="'+ this.odd1.url.toString() +'" >' + this.odd1.bookmaker + '</a>' + '\n';
    message += this.match_data.player2 + ' @ <b>' + this.odd2.odd2 + '</b> en ' + '<a href="'+ this.odd2.url.toString() +'" >' + this.odd2.bookmaker + '</a>' + '\n';
    message += '<i>Creado a las ' + this.createdAt.getHours() + ':' + this.createdAt.getMinutes() + '</i>';

    return message;
  },

});

/**
 * Statics
 */

// NotificationSchema.static({

//   searchSimilarOld : function (data, cb){
//     //@TOCHANGE This is momentary
//     this.findOne({color: data.color, product: data.product})  
//       .exec(cb);
//   },

//   searchSimilar : function (data, cb){
//     var MIN_SCORE = 15;
//     var textsearch = data.product+' '+(data.detail||'')+' '+(data.color||''); 

//     logger.debug('PRODUCT-M: Finding similar for ', data);
//     logger.debug('PRODUCT-M: Textsearch: ', textsearch);

//     this.findOne( 
//         { brand: data.brand,  
//           "links.store_id": { $nin: data.shop }, // Exclude same store products
//           $text : { $search : textsearch } }, // Search by textsearch query
//         { score : { $meta: "textScore" } } // Retrieve score & filters the best
//     )
//     .sort({ score : { $meta : 'textScore' } })
//     .exec(function(err, returned){
//       // Last filter, exclude if doesnt pass min score
//       if (returned && returned._doc.score < MIN_SCORE)
//         returned = null;

//       cb(err, returned);
//     });
      
//   },

  // search : function (params, cb) {
  //   var query = {};
  //   var by = params.by;
  //   var text = params.text;

  //   //Add regexp to the query
  //   if (text && text.length)
  //     query[by] = new RegExp(text, 'i');

  //   this.find(query)
  //       .exec(cb);
  // }, 

  // update: function (document, cb){

  // }
//});

/**
 * Register
 */

mongoose.model('SurebetCouple', SurebetSchema);


function parseSport(sport){
  switch(sport){
    case ('basket') : return '🏀🏀🏀🏀';
    break;
    case ('soccer') : return '⚽⚽⚽⚽';
    break;
    case ('tennis') : return '🎾🎾🎾🎾';
    break;
  }
}

function parseProfit(profit, params){
  var rounded = (Math.round((profit + 0.00001) * 100) / 100);
  var ret = rounded+'%';

  if (params && params.raised){
    ret = '🔝'+rounded+'%🔝';
  }
  else{
    if(rounded>0)
      ret = '✅'+rounded+'%✅';
    else if (rounded<0)
      ret = '🅾'+rounded+'%🅾';
  }

  return ret;
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}