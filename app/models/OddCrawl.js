/**
 * Requires
 */
var diac = require('../../helpers/diacritics');

/**
 * Constructor
 */
var OddCrawl = function(data) {
  //MATCH-DATA
  this.sport = data.sport;
  this.player1 = data.player1;
  this.player2 = data.player2;
  this.league = data.league;
  this.date = data.date;
  this.live = data.live;
  
  //ODDS
  this.odds = {
    url : data.odds.url,
    bookmaker : data.odds.bookmaker,
    odd1 : data.odds.odd1,
    odd2 : data.odds.odd2,
    oddt : data.odds.oddt
  }

  this.searcheable = diac.removeDiacritics((data.player1 + ' ' + data.player2).toLowerCase());
}

/**
 * Properties
 */
OddCrawl.prototype = {  

  //Adds a new PageURL
  add: function(url_s, store, cb){
    client = redis.getClient();
    if (url_s)
      client.sadd(this.TABLE_NAME+":"+store, url_s, cb);
    else
      cb(new Error('REDIS: Error, undefined producturl'));
  },

  getAll: function(store, cb){
    client = redis.getClient();
    client.smembers(this.TABLE_NAME+":"+store, cb);      
  }

};

/**
 * Exports
 */
module.exports = OddCrawl;


/**
 * Privates
 */
