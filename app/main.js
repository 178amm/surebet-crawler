
/*
 * Module dependencies.
 */
var fs = require('fs');
//var _ = require('underscore');
// var pageURL = require('./models/PageURL');
// var productURL = require('./models/ProductURL');
// var productPrice = require('./models/ProductPrice');
// var mainProducts = require('./main.products');

const path = require('path');
const logger = require('logger');
const async = require('async');
const mongoose = require('mongoose');
var Sync = require('sync');

var Match = mongoose.model('Match');
var Surebet = mongoose.model('Surebet');

var config = require ('../config');

var surebetMiddleware = new (require ('./middleware/Surebet'))(config.middleware.surebet);
var surebetCoupleMiddleware = new (require ('./middleware/SurebetCouple'))(config.middleware.surebet_couple);

/*
 * Exports
 */

exports.crawlURLsDataFromJSON = function(json_path, options, cb){

    //Parse options 
    var MAX = options.max | 0;
    var BATCH_SIZE = options.batchSize | 1;
    var SKIP = options.skip | 0;

    //Statistics
    var stats_total = 0;
    var stats_init_time = Date.now();

    if (!MAX)
        logger.info('MAIN: Crawling all urls in slices of '+BATCH_SIZE);
    else
        logger.info('MAIN: Crawling '+MAX+' url(s) in '+BATCH_SIZE+' batch(es)');

    // Read File
    var jpath = json_path ? json_path : './data/all.json';
    var URLsData = require(path.resolve(jpath));

    //Set skip
    if(SKIP){
        logger.debug('MAIN: Skipped '+SKIP+' urls');
        URLsData = URLsData.slice(SKIP, URLsData.length);
    }   
    //Set max
    if(MAX){
        logger.debug('MAIN: Array sliced')
        URLsData = URLsData.slice(0, MAX);
    }

    //Iterate asinchronously on each url data
    async.eachSeries(URLsData, function(URLData, cba){
        exports.crawlOddsFromURLData(URLData, function(err, odds){
            if (err)
                logger.error(err);
            else{ //Done
                logger.debug('Odds from url saved');
            } 
            //Dont kill async on errors
            cba(null, odds);
        });
    }, function(err){
        //Done! All pages processed
        cb(err);
    });
}


exports.crawlOddsFromURLData = function(URLData, cb){
    
    if (!URLData) //@TODO parse urlData
        cb(new Error('MAIN: URL not provided'));
    else{
        //
        var Extractor = require('./extractors/'+capitalize(URLData.extractor));
        var extractor = new Extractor();

        extractor.getOddCrawlsFromURL(URLData.url, URLData.sport, function(err, oddCrawls){
            if (err)
                cb(err);
            else{ //Save links
                logger.debug('Product urlData retrieved, items length: ' ,oddCrawls.length)
                //Save or update each odd
                async.eachSeries(oddCrawls, function(oddCrawl, cba){
                    // Sync(function(){
                        //Skip live events
                        if (!oddCrawl.live)
                            exports.upsertMatchOdd(oddCrawl, function(err, upserted_match){
                                if (err){
                                    logger.error(err);
                                }
                                else{ //Save links
                                    logger.debug('Match odds saved')//, upserted_match);
                                } 
                                //Dont kill async on errors
                                cba(null, upserted_match);
                            });
                        else
                            cba(null, null);
                    // });
                }, function(err){
                    //Done! All pages processed
                    cb(err);
                });
            }                 
        });
    }
}

exports.upsertMatchOdd = function(oddCrawl, cb){

    logger.info('Searching match ', oddCrawl.player1 + ' ' + oddCrawl.player2);
    Match.searchMatch(oddCrawl, function(err, found_match){
        if (err)
            cb(err);
        else{
            if (!found_match && oddCrawl){
                //Similar not found, save new, parse odds
                var match = new Match(oddCrawl);
                //@TODO Could notify new match
                match.save(function(err, done){
                    if (err){
                        logger.error('Error saving the given crawl: ');
                        logger.error(oddCrawl);
                    }

                    cb(err, done);
                });
            }
            else{
                found_match.upsertOdds(oddCrawl, function(err, upserted_match){
                    if (err)
                        cb(err);
                    else{
                        //@WATCHOUT Middleware mounting
                        //-----------------------------
                        surebetMiddleware.check(upserted_match.doc, function(err, data){
                            surebetCoupleMiddleware.check(upserted_match.doc, cb);
                        });
                    }
                });
            }
        }
    });
}

/*
 * Privates
 */

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//function 