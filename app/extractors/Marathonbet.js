/**
 * Requires
 */
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const logger = require('logger');
const md5 = require('md5');
const OddCrawl = require('../models/OddCrawl');

/**
 * Constructor
 */
var Marathonbet = function(config) {
  // Vars
  this.config = config;
  this.MAIN_URL = 'https://www.marathonbet.es';
  this.BOOKMAKER_ID = 'marathonbet';
}

/**
 * Properties
 */
Marathonbet.prototype = {  

  getOddCrawlsFromURL: function(URL, sport, cb){
    //Vars
    var self = this;    
    var items = [];
    //Retrieve the first one
    if (!URL)
      cb(new Error('No URL defined'));
    else{
      logger.debug('Requesting URL page...', URL);
      request(URL, function (error, response, body) {
        logger.debug('Request OK, data received');
        if (error)
          cb(error)
        else{ 
          var $;          
          try{
              // this.sport = data.sport;
              // this.player1 = data.player1;
              // this.player2 = data.player2;
              // this.league = data.league;
              // //ODDS
              // this.odd1 = data.odd1;
              // this.odd2 = data.odd2;
              // this.odd_tie = data.odd_tie;

            // Safely tries to parse object
            $ = cheerio.load(body);
            // Pase by league blocks
            $('.category-container').each(function(i, element){
              var league = $(this).find('.category-label').text().trim();
              //Iterate each
              $(this).find('.event-header').each(function(e, element){
                var item = {};
                item.sport = sport;
                //Number
                var number = Number($(this).find('.member-number').eq(0).text().trim()[0]);
                //Tries member name
                item.player1 = $(this).find('.member-name').eq(0).text().trim();
                item.player2 = $(this).find('.member-name').eq(1).text().trim();

                item.date = $(this).find('.date').text().trim();
                var urli = self.MAIN_URL+$(this).parent().attr('data-event-page');

                //Tries today's member name (also affect to the date)
                if (!item.player1 || !item.player2){
                  item.player1 = $(this).find('.today-member-name').eq(0).text().trim();
                  item.player2 = $(this).find('.today-member-name').eq(1).text().trim();
                  item.date = (new Date().getDate())+' '+getCurrentMonth()+' '+item.date;  
                }

                item.date = new Date(item.date);
                item.date.setFullYear(new Date().getFullYear());
                //item.date = new Date(Date.now());
                item.league = league;
                //Odds
                item.odds = {
                  url: urli,
                  bookmaker: self.BOOKMAKER_ID,
                  odd1: $(this).find('.price.height-column-with-price').eq(0).text().trim(),
                  oddt: $(this).find('.price.height-column-with-price').eq(1).text().trim(),
                  odd2: $(this).find('.price.height-column-with-price').eq(2).text().trim()
                }
                //Check switch players & odds position
                if (number==2){
                  var oddaux = item.odds.odd1;
                  var plaux = item.player1;

                  item.player1 = item.player2;
                  item.player2 = plaux;
                  item.odds.odd1 = item.odds.odd2;
                  item.odds.odd2 = oddaux;
                }

                items.push(new OddCrawl(item));
              });
            });
          }
          catch(e){
            error = e;
          }
          if (error)
            cb(error);
          else{
            //Build            
            logger.debug('Elements result');
            cb(null, items);
          }
        }
      });
    }    
  }

};

/**
 * Exports
 */
module.exports = Marathonbet;


/**
 * Privates
 */
function parsePrice(raw){
  return parseFloat(raw.slice(0,-1).replace(',', ''));
}

function parseProductCrawl(rawProduct){
  //shop
  //brand
  //url
  //product
  //detail
  //color
  //price
  //store_id
}

function getCurrentMonth(){
  var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  return monthNames[(new Date()).getMonth()];
}