/**
 * Requires
 */
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const logger = require('logger');
const md5 = require('md5');

/**
 * Constructor
 */
var Marathonbet = function(config) {
  // Vars
  this.config = config;
  this.MAIN_URL = 'https://www.marathonbet.es';
  this.SHOP_ID = 'marathonbet';
}

/**
 * Properties
 */
Marathonbet.prototype = {  

  getOddCrawlsFromURL: function(URL, sport, cb){
    //Vars
    var self = this;    
    var items = [];
    //Retrieve the first one
    if (!URL)
      cb(new Error('No URL defined'));
    else{
      logger.debug('Requesting URL page...', URL);
      request(URL, function (error, response, body) {
        logger.debug('Request OK, data received');
        if (error)
          cb(error)
        else{ 
          var $;          
          try{
              // this.sport = data.sport;
              // this.player1 = data.player1;
              // this.player2 = data.player2;
              // this.league = data.league;
              // //ODDS
              // this.odd1 = data.odd1;
              // this.odd2 = data.odd2;
              // this.odd_tie = data.odd_tie;

            // Safely tries to parse object
            $ = cheerio.load(body);
            //Parse by league blocks
            $('.gl-MarketGroup.cm-CouponMarketGroup.cm-CouponMarketGroup_Open').each(function(i, element){
              var league = $(this).find('.cm-CouponMarketGroupButton_Text').text().trim();
              logger.info(league);
              //Get 1x2 odds row
              var sl = '.sl-CouponParticipantWithBookCloses.sl-CouponParticipantIPPGBase.sl-MarketCouponAdvancedBase_LastChild'
              $(sl).each(function(i, element){
                logger.info($(this).text().trim());
              });

            //   //Parse by date block (players & times -row)
            //   $(this).find('.event-header').each(function(e, element){
            //     var item = {};
            //     item.sport = sport;
            //     item.player1 = $(this).find('.member-name').eq(0).text().trim();
            //     item.player2 = $(this).find('.member-name').eq(1).text().trim();
            //     item.date = new Date($(this).find('.date').text().trim());
            //     item.date.setFullYear(new Date().getFullYear());
            //     //item.date = new Date(Date.now());
            //     item.league = league;
            //     item.odd1 = $(this).find('.price.height-column-with-price').eq(0).text().trim();
            //     item.oddt = $(this).find('.price.height-column-with-price').eq(1).text().trim();
            //     item.odd2 = $(this).find('.price.height-column-with-price').eq(2).text().trim();
            //     items.push(item);
            //   });
            });
          }
          catch(e){
            error = e;
          }
          if (error)
            cb(error);
          else{
            //Build            
            logger.debug('Elements result');
            cb(null, items);
          }
        }
      });
    }    
  }

};

/**
 * Exports
 */
module.exports = Marathonbet;


/**
 * Privates
 */
function parsePrice(raw){
  return parseFloat(raw.slice(0,-1).replace(',', ''));
}

function parseProductCrawl(rawProduct){
  //shop
  //brand
  //url
  //product
  //detail
  //color
  //price
  //store_id
}

