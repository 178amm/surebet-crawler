/**
 * Requires
 */
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const logger = require('logger');
const md5 = require('md5');
const OddCrawl = require('../models/OddCrawl');

/**
 * Constructor
 */
var E888sport = function(config) {
  // Vars
  this.config = config;
  this.MAIN_URL = 'https://www.888sport.es';
  this.BOOKMAKER_ID = '888sport';
}

/**
 * Properties
 */
E888sport.prototype = {  

  getOddCrawlsFromURL: function(URL, sport, cb){
    //Vars
    var self = this;    
    var items = [];
    //Retrieve the first one
    if (!URL)
      cb(new Error('No URL defined'));
    else{
      logger.debug('Requesting URL page...', URL);
      request(URL, function (error, response, body) {
        logger.debug('Request OK, data received');
        if (error)
          cb(error)
        else{ 
          var $;          
          try{
            // Safely tries to parse object
            $ = cheerio.load(body);
            // Pase by event blocks
            logger.debug('Starting extraction');
            $('.KambiBC-event-item').each(function(i, element){
              //Only way to check if its a win/lose event
              var oddsAndPlayers = $(this).find('.KambiBC-bet-offer__outcomes');
              logger.debug('Finding oddsandplayers');

              
              if (oddsAndPlayers){
                logger.debug('Debugging item');
                var item = {};

                item.sport = sport;
                item.league = $(this).find('.KambiBC-event-participants__name').text().trim();

                var url = URL + $(this).children.first().prop('href').trim();
                var date = $(this).find('.KambiBC-event-item__start-time--date').text().trim();
                var time = $(this).find('.KambiBC-event-item__start-time--time').text().trim();

                item.date = new Date(date+time);
                item.date.setFullYear(new Date().getFullYear());

                item.player1 = $(oddsAndPlayers).find('.KambiBC-mod-outcome__label').eq(0).text().trim();
                item.player2 = $(oddsAndPlayers).find('.KambiBC-mod-outcome__label').eq(1).text().trim();

                item.odds = {
                  url: url,
                  bookmaker: self.BOOKMAKER_ID,
                  odd1: $(oddsAndPlayers).find('.KambiBC-mod-outcome__odds').eq(0).text().trim(),
                  //oddt: $(this).find('.price.dec').eq(1).text().trim(),
                  odd2: $(oddsAndPlayers).find('.KambiBC-mod-outcome__odds').eq(1).text().trim()
                }

                items.push(new OddCrawl(item));
              }
            });
          }
          catch(e){
            error = e;
          }
          if (error)
            cb(error);
          else{
            //Build            
            logger.debug('Elements result');
            cb(null, items);
          }
        }
      });
    }    
  }

};

/**
 * Exports
 */
module.exports = E888sport;


/**
 * Privates
 */
function parsePrice(raw){
  return parseFloat(raw.slice(0,-1).replace(',', ''));
}

function parseProductCrawl(rawProduct){
  //shop
  //brand
  //url
  //product
  //detail
  //color
  //price
  //store_id
}

