/**
 * Requires
 */
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const logger = require('logger');
const md5 = require('md5');
const OddCrawl = require('../models/OddCrawl');

/**
 * Constructor
 */
var Williamhill = function(config) {
  // Vars
  this.config = config;
  this.MAIN_URL = 'http://sports.williamhill.es/bet_esp/es';
  this.BOOKMAKER_ID = 'williamhill';
}

/**
 * Properties
 */
Williamhill.prototype = {  

  getOddCrawlsFromURL: function(URL, sport, cb){
    //Vars
    var self = this;    
    var items = [];
    //Retrieve the first one
    if (!URL)
      cb(new Error('No URL defined'));
    else{
      logger.debug('Requesting URL page...', URL);
      request(URL, function (error, response, body) {
        logger.debug('Request OK, data received');
        if (error)
          cb(error)
        else{ 
          var $;          
          try{
              // this.sport = data.sport;
              // this.player1 = data.player1;
              // this.player2 = data.player2;
              // this.league = data.league;
              // //ODDS
              // this.odd1 = data.odd1;
              // this.odd2 = data.odd2;
              // this.odd_tie = data.odd_tie;

            // Safely tries to parse object
            $ = cheerio.load(body);
            logger.info('Searching in williamhill alt')
            // Pase by league blocks
            //logger.debug('Length', $('#_sport_0_types').children().length);
            var league = $('h1').last().text().trim();
              logger.debug('For league: ', league);
              if(league){
                //Iterate each match
                logger.debug('Iterating n elements:', $('tbody>tr').length )

                $('tbody>tr').each(function(e, element){
                  var item = {};
                  //Get url
                  var url = $(this).children().eq(2).find('a').attr('href');
                  //If its a live event
                  var date = $(this).children().eq(0).text().trim();
                  if (date.length<4 || date == 'Directo'){
                    item.live = true
                    item.date = new Date(); // Now
                  }
                  //Not a live event, build date
                  else{
                    var time = $(this).children().eq(1).text().trim().slice(0, -2);
                    item.date = new Date(date + ' ' + time);
                    item.date.setFullYear(new Date().getFullYear());
                    item.live = false;
                  }
                  //Build teams
                  var teams = $(this).children().eq(2).text().trim().split('₋');
                  if (teams.length==1){
                    teams = cleanupTeamsScores(teams[0]);
                    teams = teams.split('-');
                  }                                                      
                  item.player1 = teams[0].trim().replace(/ /g, "").replace(/ /g, "");
                  item.player2 = teams[1].trim().replace(/ /g, "").replace(/ /g, "");
                  //Build rest of the data
                  item.sport = sport;
                  item.league = league;
                  item.odds = {
                    url: url,
                    bookmaker: self.BOOKMAKER_ID,
                    odd1: $(this).children().eq(4).text().trim(),
                    oddt: $(this).children().eq(5).text().trim(),
                    odd2: $(this).children().eq(6).text().trim()
                  }

                  items.push(new OddCrawl(item));
                });
              }
          }
          catch(e){
            error = e;
            logger.error(e);
          }
          if (error)
            cb(error);
          else{
            //Build            
            logger.debug('Elements result');
            cb(null, items);
          }
        }
      });
    }    
  }

};

/**
 * Exports
 */
module.exports = Williamhill;


/**
 * Privates
 */
function parsePrice(raw){
  return parseFloat(raw.slice(0,-1).replace(',', ''));
}

function parseProductCrawl(rawProduct){
  //shop
  //brand
  //url
  //product
  //detail
  //color
  //price
  //store_id
}

function cleanupTeamsScores(team){
  team = team.replace(/\n/g, "");
  team = team.replace(/\t/g, "");
  return team.split('(')[0] + team.split(')')[1];
}