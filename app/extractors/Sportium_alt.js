/**
 * Requires
 */
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const logger = require('logger');
const md5 = require('md5');
const OddCrawl = require('../models/OddCrawl');

/**
 * Constructor
 */
var Sportium = function(config) {
  // Vars
  this.config = config;
  this.MAIN_URL = 'https://www.marathonbet.es';
  this.BOOKMAKER_ID = 'sportium';
}

/**
 * Properties
 */
Sportium.prototype = {  

  getOddCrawlsFromURL: function(URL, sport, cb){
    //Vars
    var self = this;    
    var items = [];
    //Retrieve the first one
    if (!URL)
      cb(new Error('No URL defined'));
    else{
      logger.debug('Requesting URL page...', URL);
      request(URL, function (error, response, body) {
        logger.debug('Request OK, data received');
        if (error)
          cb(error)
        else{ 
          var $;          
          try{
              // this.sport = data.sport;
              // this.player1 = data.player1;
              // this.player2 = data.player2;
              // this.league = data.league;
              // //ODDS
              // this.odd1 = data.odd1;
              // this.odd2 = data.odd2;
              // this.odd_tie = data.odd_tie;

            // Safely tries to parse object
            $ = cheerio.load(body);
            // Pase by league blocks
            $('.expander').each(function(i, element){
              var league = $(this).find('.expander-button').text().trim();
              //Iterate each
              $(this).find('.mkt.mkt_content').each(function(e, element){
                var item = {};
                item.sport = sport;
                var url = URL; //only way
                item.player1 = $(this).find('.seln-label').eq(0).text().trim();
                item.player2 = $(this).find('.seln-label').eq(1).text().trim();
                var date_str = {
                  time: $(this).find('span.time').text().trim(),
                  date: $(this).find('span.date').text().trim(),
                }
                //item.dt = date_str;
                item.date = new Date(date_str.date + ' ' + date_str.time);
                item.date.setFullYear(new Date().getFullYear());
                //item.date = new Date(Date.now());
                item.league = league;
                item.odds = {
                  url: url,
                  bookmaker: self.BOOKMAKER_ID,
                  odd1: $(this).find('.price.dec').eq(0).text().trim(),
                  odd2: $(this).find('.price.dec').eq(1).text().trim(),
                  //odd2: $(this).find('.price.dec').eq(2).text().trim()
                }

                items.push(new OddCrawl(item));
              });
            });
          }
          catch(e){
            error = e;
          }
          if (error)
            cb(error);
          else{
            //Build            
            logger.debug('Elements result');
            cb(null, items);
          }
        }
      });
    }    
  }

};

/**
 * Exports
 */
module.exports = Sportium;


/**
 * Privates
 */
function parsePrice(raw){
  return parseFloat(raw.slice(0,-1).replace(',', ''));
}

function parseProductCrawl(rawProduct){
  //shop
  //brand
  //url
  //product
  //detail
  //color
  //price
  //store_id
}

