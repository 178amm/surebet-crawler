/**
 * Requires
 */
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const logger = require('logger');
const md5 = require('md5');
const OddCrawl = require('../models/OddCrawl');

/**
 * Constructor
 */
var Betfair = function(config) {
  // Vars
  this.config = config;
  this.MAIN_URL = 'https://www.betfair.es';
  this.BOOKMAKER_ID = 'betfair';
}

/**
 * Properties
 */
Betfair.prototype = {  

  getOddCrawlsFromURL: function(URL, sport, cb){
    //Vars
    var self = this;    
    var items = [];
    //Retrieve the first one
    if (!URL)
      cb(new Error('No URL defined'));
    else{
      logger.debug('Requesting URL page...', URL);
      request(URL, function (error, response, body) {
        logger.debug('Request OK, data received');
        if (error)
          cb(error)
        else{ 
          var $;          
          try{
              // this.sport = data.sport;
              // this.player1 = data.player1;
              // this.player2 = data.player2;
              // this.league = data.league;
              // //ODDS
              // this.odd1 = data.odd1;
              // this.odd2 = data.odd2;
              // this.odd_tie = data.odd_tie;

            // Safely tries to parse object
            $ = cheerio.load(body);
            // Pase by league blocks
            $('.section').each(function(i, element){
              var league = $(this).find('.section-header.section-header-new-layout').text().trim();
              //Iterate each
              $(this).find('.com-coupon-line-new-layout').each(function(e, element){
                var coupon = this;
                var item = {};

                //Gets url link
                var url = self.MAIN_URL + $(this).find('.event-name-info>a').prop('href').trim();

                //Tries to parse date
                item.date = $(this).find('.date.ui-countdown').text().trim();
                //If tomorrow
                if (item.date.indexOf('Mañana')>=0){
                  logger.info('MAÑANA', item.date.split('Mañana'));
                  var time = item.date.split('Mañana')[1].trim();
                  item.date = new Date();
                  item.date.setHours(time.split(':')[0])
                  item.date.setMinutes(time.split(':')[1]);
                  item.date.setSeconds(0);
                  item.date.setMilliseconds(0);
                  item.date.setDate(item.date.getDate()+1);
                }
                // Only time
                else if (item.date.length == 5){
                  var time = item.date.trim();
                  item.date = new Date();
                  item.date.setHours(time.split(':')[0])
                  item.date.setMinutes(time.split(':')[1]);
                  item.date.setSeconds(0);
                  item.date.setMilliseconds(0);
                }
                else if (item.date.indexOf('Comienza')>=0){
                  item.date = new Date();
                  item.live = true; 
                }
                else
                  item.date = new Date(item.date);

                item.date.setFullYear(new Date().getFullYear());

                item.sport = sport;
                item.player1 = $(this).find('.team-name').eq(0).text().trim();
                item.player2 = $(this).find('.team-name').eq(1).text().trim();

                // logger.debug(item.player1, item.player2);
                // $(this).find('.ui-runner-price').each(function(e, element){
                //   logger.debug($(this).text().trim());
                // })

                item.league = league;
                var scoremarker = $(this).find('.details-market.market-2-runners').eq(2);
                item.odds = {
                  url: url,
                  bookmaker: self.BOOKMAKER_ID,
                  odd1: $(scoremarker).find('.ui-runner-price').eq(0).text().trim(),
                  //oddt: $(this).find('.price.dec').eq(1).text().trim(),
                  odd2: $(scoremarker).find('.ui-runner-price').eq(1).text().trim()
                }

                items.push(new OddCrawl(item));
              });
            });
          }
          catch(e){
            error = e;
          }
          if (error)
            cb(error);
          else{
            //Build            
            logger.debug('Elements result');
            cb(null, items);
          }
        }
      });
    }    
  }

};

/**
 * Exports
 */
module.exports = Betfair;


/**
 * Privates
 */
function parsePrice(raw){
  return parseFloat(raw.slice(0,-1).replace(',', ''));
}

function parseProductCrawl(rawProduct){
  //shop
  //brand
  //url
  //product
  //detail
  //color
  //price
  //store_id
}

