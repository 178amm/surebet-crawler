/**
 * Requires
 */
const logger = require('logger');
const mongoose = require('mongoose');
const config = require('../../config');
const telegram = new (require ('../../helpers/Telegram'))(config.telegram);

const SurebetCoupleM = mongoose.model('SurebetCouple');

/**
 * Constructor
 */
var SurebetCouple = function(config) {
  // Vars
  this.config = config;
}

/**
 * Properties
 */
SurebetCouple.prototype = {  

  check : function (match, cb) {

    var self = this;
    var broadcast = null;

    // Check there is more than a single bookmaker
    if (match.odds.length > 1){

      var profit = calculateProfitability(match, self.config);

      logger.debug('Profitability couple', profit);

      // Check coef is a surebet
      if (profit && profit.coef > self.config.min_profit) {

        // Create surebet
        var surebet = new SurebetCoupleM({
          match_data: match,
          profitability: profit.coef,
          odd1: profit.odd1,
          odd2: profit.odd2,
        });

        // Check doesnt already exists 
        SurebetCoupleM.findOne({ "match_data._id": match._id }).sort({createdAt: -1}).exec(
          function(err, surebet_found){

            logger.debug('SUREBET COUPLE FOUND', surebet);

            if (err)
              cb(err)            
            //Exists, check if new coef is lower
            else if (!surebet_found){
              logger.debug('SUREBET COUPLE NOT FOUND, BROADCASTING...', surebet_found);
              saveAndBroadcast(surebet, null, cb);
            }
            else if (surebet_found && Number(surebet.profitability) > Number(surebet_found.profitability)){
              logger.debug('SUREBET COUPLE FOUND, BIGGER Profitability', surebet_found);
              saveAndBroadcast(surebet, { raised: true }, cb);
            }
            else
              cb(null)
          });

        function saveAndBroadcast(surebet, params, cb){
          if (self.config.broadcast)
            telegram.broadcast(self.config.channel_id, surebet.toHTML(params));
          // Save surebet
          surebet.save(function(err, data){
            if (err)
              logger.error(err);
            
            cb(err, data);
          });
        }
      }
      else
        cb(null);
    }
    else
      cb(null);
  }
};

/**
 * Exports
 */
module.exports = SurebetCouple;


/**
 * Privates
 */

function calculateProfitability(match, couple){

  //@TODO parse couple
  
  var odds = JSON.parse(JSON.stringify(match.odds));

  // // Gets index of max odd
  // var map1 = odds.map(function(o){return o.odd1;});
  // var m1 = map1.indexOf(Math.max(...map1));
  // var d1 = odds[m1];
  // // Delete found bookmaker since both shouldt match
  // odds.splice(m1, 1);
  // // Re-search
  // var map2 = odds.map(function(o){return o.odd2;});
  // var m2 = map2.indexOf(Math.max(...map2));
  // var d2 = odds[m2];

  var d1 = odds.find(x => x.bookmaker === couple.bookmaker1);
  var d2 = odds.find(x => x.bookmaker === couple.bookmaker2);
  var profit;

  if (d1 && d2)
    profit = {
      odd1: d1,
      odd2: d2,
      coef: ((1-(1/d1.odd1 + 1/d2.odd2 ))*100)
    }

  return profit;
}