/**
 * Requires
 */
const request = require('request');
const logger = require('logger');

/**
 * Constructor
 */
var Telegram = function(config) {
  // Vars
  this.config = config;

  this.botToken = config.surebetter_bot.token;
  this.defaultURL = 'https://api.telegram.org'+'/bot'+this.botToken+'/';
  this.channel_name = config.channel_name;

}

/**
 * Properties
 */
Telegram.prototype = {  

  broadcast: function(channel_id, message, cb){
    //Vars
    var self = this;    
    //Retrieve the first one
    if (!message)
      cb(new Error('No message defined'));
    else{
      logger.debug('Requesting URL page...', URL);

      var URL = self.defaultURL + 'sendMessage';
      var querystring = { chat_id: channel_id, text: message, parse_mode: 'HTML'}

      request({ url: URL, qs: querystring }, function (error, response, body) {
        if (cb){
          if (error)
            cb(error)
          else
            cb(null, response)
        }
        else{
          if (error)
            logger.error('TELEGRAM API ERROR:', error);
          else{
            logger.debug('Message broadcast', body);
          }
        }
      });
    }    
  }

};

/**
 * Exports
 */
module.exports = Telegram;


/**
 * Privates
 */
function parsePrice(raw){
  return parseFloat(raw.slice(0,-1).replace(',', ''));
}

function parseProductCrawl(rawProduct){
  //shop
  //brand
  //url
  //product
  //detail
  //color
  //price
  //store_id
}