//Module dependencies
var bunyan = require('bunyan');
var log = bunyan.createLogger(
	{
		name: 'requester',   
		streams:
			[{
		      stream: process.stdout,
		      level: 'debug'
		    }]				
	});

//Funcs
exports.info = function (message, object) {
	if(!object)
		log.info(message);
	else
		log.info(message, object);
}

exports.debug = function (message, object){
	if(!object)
		log.debug(message);
	else
		log.debug(message, object);
}

exports.error = function (err){
	log.error(err);
}