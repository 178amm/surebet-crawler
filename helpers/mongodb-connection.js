/*
 * Requires
 */
var mongoose = require("mongoose");
var logger = require('logger');


/*
 * Private
 */
var client = undefined;

// function sum(num1, num2) {
//   return num1 + num2;
// }


/*
 * Public
 */
var self = module.exports = {

  getClient: function getClient(){
  	return client;
  },		
  
  connect: function connect(config_url, cb){
  	if (!client){

      var default_url = 'mongodb://test:test@localhost:27017/surebet-crawler';
      
      mongoose.connection.on('error', function (err) {
        logger.error("MONGODB: Error " + err);
        cb(err);
      });

      mongoose.connection.on('connected', function(data){
        logger.info('MONGODB: Connection stablished with '+mongoose.connections[0].host);
        logger.info('MONGODB: DDBB Ready');
        cb(null, client);
      });

      mongoose.connect(config_url || default_url, function(err){
        if (err)
          cb(err)
        else{
          client = mongoose;
        }
      });    
  	}
    else
    	cb(new Error('MONGODB - Error, client already connected'));
  },
  
  disconnect: function disconnect(cb){
    if (!client)
      cb(new Error('MONGODB: Cant disconnect, not client defined'));
    else{
      client.disconnect(cb);
    }
  }
};