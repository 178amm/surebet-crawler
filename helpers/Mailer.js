/**
 * Requires
 */
var nodemailer = require('nodemailer');
var config = require('../../config');
var path = require('path');

/**
 * Constructor
 */
var Mailer = function(config) {
  // Vars
  this.config = config;  
  var self = this;
}

/**
 * Properties
 */
Mailer.prototype = {  

  //Watches for files & email'em
  send: function(options, cb) {    

  	//Options format
  	// {
  	// 	subject: 'This is a subject',
  	// 	body: 'Body',
  	// 	body_html: '<b>Body<b>',
  	// 	filepath: 'c:/demo/demo.pdf'
  	// }

  	//Define params
  	var config = this.config;
	var mailOptions = {
	    from: options.subject + ' <'+config.auth.user+'>', // sender address 
	    to: config.to, // list of receivers 
	    subject: options.subject, // Subject line 
	    text: options.body, // plaintext body 
	    html: options.body_html, // html body 
	    attachments: [
	        // {   // file on disk as an attachment
	        //     filename: path.basename(options.file_path),
	        //     path: options.file_path // stream this file
	        // },
	        {
	        	filename: path.basename(options.file_path),
	            content: options.file_buffer // stream this file    	
	        }
        ]
	};
 
	// Send email
	var transporter = nodemailer.createTransport(config);
	console.log('Sending mail...')
	transporter.sendMail(mailOptions, function(err, data){
	    if(err){
	    	console.log('Error sending mail');
	    	cb(err);
	    }
	    else{
	    	console.log('Message sent: ' + data.response);
	    	cb(null, data);
	    }	    
	});
  },

};



/**
 * Exports
 */
module.exports = Mailer;


/**
 * Privates
 */