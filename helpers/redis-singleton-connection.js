/*
 * Requires
 */
var redis = require("redis");
var logger = require("logger");


/*
 * Private
 */
var client = undefined;

// function sum(num1, num2) {
//   return num1 + num2;
// }


/*
 * Public
 */
var self = module.exports = {

  getClient: function getClient(){
  	return client;
  },		
  
  connect: function connect(config_url, cb){
  	if (!client){

      //var conf_url = 'redis://adriandev:2eaf4dedff409dfe448a7a210936280e@50.30.35.9:3650/';
      var conf_url = config_url || 'redis://test:test@localhost:6379/';

    	client = redis.createClient(conf_url);

    	client.on('error', function (err) {
		    logger.error("REDIS - Error ", err);
        cb(err);
      });

		  client.on('connect', function(data){
			 logger.info('REDIS - Connection stablished with ' + client.connection_options.host);
		  });

		  client.on('ready', function(data){
			 logger.info('REDIS - DDBB Ready');
       cb(null, client);
		  });
  	}
    else
    	cb(new Error('Error, client already connected'));
  },
  
  disconnect: function disconnect(cb){
    if (!client)
      cb(new Error('REDIS: Cant disconnect, not client defined'));
    else{
      client.quit();
      cb(null);
    }
  }
};