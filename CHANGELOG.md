--------------------------------
v0.1.0
--------------------------------
- Crawls basket surebets from Betfair, Marathonbet, Sportium and Williamhill
- Broadcasting to Telegram integrated

--------------------------------
v0.2.0
--------------------------------
- Added extractors 888sports


--------------------------------
TO-D0
--------------------------------
- Throw error when no product is found in a page
- Mailer integration
- Multithread implementation
- Similar product search sould work with a dictionary
- Fix some unconsistency issues between PriceHash & PriceKeys


db.accommodations.find( { $where: "this.odds.length > 1" } );