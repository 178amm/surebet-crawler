'use strict';

/*
 * surebet-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

/**
 * Module dependencies
 */
const apppath = require('app-module-path').addPath(__dirname + '/helpers');
const redis = require('./helpers/redis-singleton-connection');
const mongodb = require('./helpers/mongodb-connection');
const config = require('./config');

const program = require('commander');
const logger = require('logger');
const async = require('async');
//const timeout = require('timeout');

 /**	
 * Bootstrap Mongoose models
 */
require ('./app/models/Match');
require ('./app/models/Surebet');
require ('./app/models/SurebetCouple');
//require ('./app/models/Notification');

 /**
 * Bootstrap App files
 */
const main = require('./app/main');
const pjson = require('./package.json');

/**
 * DDBB Connect
 */
async.parallel([
	//Connect to the REDIS server
	//function(cb){redis.connect(config.ddbb.redis.url, cb)},
    //Connect to the MONGODB server
    function(cb){mongodb.connect(config.ddbb.mongodb.url, cb)},
//Once done, start running
], function(err, data){
	if (err)
		logger.error(err);
	else //Run
		run(function(err, data){
			//Run finished, disconnect
			logger.info('RUN: All ended, disconnecting in 5 seconds..');
			setTimeout(function(){
				logger.info('REDIS:', 'Disconnect..');
				//redis.disconnect(function(){});
				logger.info('MONGODB:', 'Disconnect..');
				mongodb.disconnect(function(){});
			}, 5000);
		});
});


/**
 * Run
 */
function run(cb){

	logger.info('Running crawler...');
	logger.info('Enviroment:', process.env.NODE_ENV || 'development');

	// program
	//   .version(pjson.version)
	//   .option('-c, --category-pages <store>', 'Crawl category pages from a json file, stored at data/<store>.categories.json,', main.crawlCategoryPagesFromJSON)
	//   .option('-p, --product-pages <store>', 'Crawl product pages from category pages stored on the ddbb', main.crawlProductFromCategoryPages)
	//   .option('-P, --products <store>', 'Crawl product from product pages stored on the ddbb', main.crawlProductsPricesFromProductURLs)
	//   .parse(process.argv);

	// Odds
	program
	  .command('odds [json]')
	  .alias('o')
	  .description('Crawl product pages from category pages stored on the ddbb')
	  .option("-m, --max <number>", "Maximum products to crawl (default all)")
	  .option("-s, --skip <number>", "Products to skip when crawling (default 0)")
	  .option("-b, --batch-size <number>", "Products to crawl per batch (default 1)")
	  .option("-c, --cron-mode <number>", "Enables cron mode. Starts to crawl infinitely for a given interval (default 1(sec))")
	  .action(function(jsonp, options){
	  	//
	  	logger.info('Crawling odds...');
	  	logger.info('Enviroment: ' + (process.env.NODE_ENV ? process.env.NODE_ENV.toUpperCase() : 'DEVELOPMENT'));
	  	logger.info('Cron mode: ' + options.cronMode + ' segs')
	  	main.crawlURLsDataFromJSON(jsonp, options, function(err, data){
	  		logger.info('Crawling odds done!');
	  		if (options.cronMode) {
	  			function endlessCrawl(){
	  				logger.info('Starting again...');
	  				main.crawlURLsDataFromJSON(jsonp, options, function(err, data){
	  					logger.info('Crawling odds done!');
	  				});
	  			}
				setInterval(endlessCrawl, options.cronMode*1000);
	  		}
	  		else	  		
	  			cb(err, data);	  		
	  	});
	  });

	 // Unknown command
	 program
	  .command('*')
	  .action(function(env){
	    logger.error('Error, command not found', env);
	    cb(null, null);
	  });

	program.parse(process.argv, function(err, data){
		logger.debug('Parsed finish');
	});
}

