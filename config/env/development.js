/**
 * Expose
 */

module.exports = {
 	ddbb:{
	    mongodb:{
	    	url: 'mongodb://test:test@localhost:27017/surebet-crawler'
	    }
  	},
	telegram: {
		surebetter_bot: {
			token: '336437808:AAF1JA7o4HCGeaNDdwC9hGgZHfTTYxVxJlE'
		}
	},
	middleware: {
		surebet: {
			min_profit: -3,
			broadcast: false,
			channel_id: '-1001140685256'
		},
		surebet_couple: {
			min_profit: -5,
			broadcast: false,
			bookmaker1: 'marathonbet',
			bookmaker2: 'williamhill',
			channel_id: '-1001128228836'
		}
	}
};
