'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

 /**
 * Bootstrap Mongoose models
 */


/**
 * Module dependencies
 */
var path = require('path');
var apppath = require('app-module-path').addPath(path.resolve(__dirname, '../node_modules_custom'));
require ('../app/models/Product');
require ('../app/models/Notification');
var mainProducts = require('../app/main.products.js');

const redis = require('../node_modules_custom/redis-singleton-connection');
const mongodb = require('../node_modules_custom/mongodb-connection');
	
const logger = require('logger');
const async = require('async');
const mongoose = require('mongoose');

var Product = mongoose.model('Product');


/**
 * DDBB Connect
 */
async.parallel([
	//Connect to the REDIS server
	function(cb){redis.connect(null, cb)},
    //Connect to the MONGODB server
    function(cb){mongodb.connect(null, cb)},
//Once done, start running
], function(err, data){
	if (err)
		logger.error(err);
	else //Run
		run(function(err, data){
			//Run finished, disconnect
			logger.info('REDIS', 'Disconnect..');
			redis.disconnect(function(){});
			logger.info('MONGODB', 'Disconnect..');
			mongodb.disconnect(function(){});
		});
});


/**
 * Run
 */
function run(cb){

	console.log('Running...');

	// var product = {
	// 	shop: 'http://www.sephora.eso',
	// 	brand: 'Zoeva',
	// 	product: 'Spectrum Strobe Palette',
	// 	color: 'Winter'
	// }

	// var product = { shop: 'primor_eu',
 //      brand: 'Yves Saint Laurent',
 //      url: 'http://www.primor.eu/yves-saint-laurent/41685-babydoll-kiss-blush-duo-stick.html',
 //      product: 'BabyDoll Kiss & Blush Duo Stick',
 //      price: 2595,
 //      color: '05',
 //      product_store_id: '30ec338d81d4df6a4c2fc6bab7397268' }

	var product = { shop: 'primor_eu', //'sephora_es',//'primor_eu',
      brand: 'Yves Saint Laurent',
      url: 'http://www.primor.eu/yves-saint-laurent/41685-babydoll-kiss-blush-duo-stick.html',
      product: 'BabyDoll Kiss & Blush Duo Stick',
      price: 2595,
      color: '04',
      product_store_id: '30ec338d81d4df6a4c2fc6bab7397268' }


	Product.searchSimilarAndUpdateLinks(product, function(err, product_updated){
		if(err)
			logger.error(err);
		else{
			logger.debug('Products updated', product_updated);
			//logger.debug(products_similar.detail);
		}
		cb(null);
	});

}