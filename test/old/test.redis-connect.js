'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

/**
 * Module dependencies
 */
var redis = require('../node_modules_custom/redis-singleton-connection');
var pageURL = require('../app/models/PageURL');
/**
 * Run
 */

console.log('Running...');



redis.connect(null, function(err, data){
	if (err)
		console.log(err);
	else{
		//Sample insert
		//console.log(redis.getClient());
		pageURL.add('thisisatest');
	}
});

