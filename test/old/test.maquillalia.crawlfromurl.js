'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

 /**
 * Bootstrap Mongoose models
 */


/**
 * Module dependencies
 */
var path = require('path');
var apppath = require('app-module-path').addPath(path.resolve(__dirname, '../node_modules_custom'));
require ('../app/models/Product');
require ('../app/models/Notification');
var mainProducts = require('../app/main.products.js');

const redis = require('../node_modules_custom/redis-singleton-connection');
const mongodb = require('../node_modules_custom/mongodb-connection');

const logger = require('logger');
const async = require('async');



/**
 * DDBB Connect
 */
async.parallel([
	//Connect to the REDIS server
	function(cb){redis.connect(null, cb)},
    //Connect to the MONGODB server
    function(cb){mongodb.connect(null, cb)},
//Once done, start running
], function(err, data){
	if (err)
		logger.error(err);
	else //Run
		run(function(err, data){
			//Run finished, disconnect
			logger.info('REDIS', 'Disconnect..');
			redis.disconnect(function(){});
			logger.info('MONGODB', 'Disconnect..');
			mongodb.disconnect(function(){});
		});
});


/**
 * Run
 */
function run(cb){

	console.log('Running...');
	var url = 'https://www.maquillalia.com/beauty-formulas-toallitas-limpiadoras-para-gafas-p-28187.html';
	var parser_name = 'maquillalia';
	//var url = 'http://www.sephora.es/Maquillaje/Rostro/Blush/Spectrum-Palette-Blush-Paleta-de-coloretes/P2775044';
	//var parser_name = 'sephora';

	// Start script
	mainProducts.crawlProductPricesFromURL(url, parser_name, function(err, data){
		if (err){
			logger.error(err);
			cb(err);
		}
		else{
			logger.info('Finished');
			cb(null, data);
		}
	});

}