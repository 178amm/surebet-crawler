'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

/**
 * Module dependencies
 */

var path = require('path');
var apppath = require('app-module-path').addPath(path.resolve(__dirname, '../node_modules_custom'));
require ('../app/models/Product');
require ('../app/models/Notification');
var mainProducts = require('../app/main.products.js');

const redis = require('../node_modules_custom/redis-singleton-connection');
const mongodb = require('../node_modules_custom/mongodb-connection');

const logger = require('logger');
const async = require('async');

var Maquillalia = require('../app/extractors/Maquillalia');
//var categories_list = require('./mocks/Maquillalia.categories.json');
//console.log(categories_list);



/**
 * Run
 */

console.log('Running...');
var extractor = new Maquillalia();

//var url = 'https://www.maquillalia.com/base-maquillaje-polvo-c-1678_16_1596_1603.html'
var url = 'https://www.maquillalia.com/toallitas-antibrillos-c-1678_16_1071.html';
//var url = 'https://www.maquillalia.com/base-maquillaje-mousse-c-1678_16_1596_1601.html'

// Start script
extractor.getProductURLsFromCategoryPageURL(url, function(err, data){
	if (err)
		logger.error(err);
	else
		logger.info(data);
});
