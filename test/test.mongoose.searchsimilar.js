'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

 /**
 * Bootstrap Mongoose models
 */


/**
 * Module dependencies
 */
var path = require('path');
var apppath = require('app-module-path').addPath(path.resolve(__dirname, '../helpers'));
require ('../app/models/Match');
//require ('../app/models/Notification');
//var mainProducts = require('../app/main.products.js');

//const redis = require('../node_modules_custom/redis-singleton-connection');
const mongodb = require('../helpers/mongodb-connection');
	
const logger = require('logger');
const async = require('async');
const mongoose = require('mongoose');

var Match = mongoose.model('Match');


/**
 * DDBB Connect
 */
async.parallel([
	//Connect to the REDIS server
	//function(cb){redis.connect(null, cb)},
    //Connect to the MONGODB server
    function(cb){mongodb.connect(null, cb)},
//Once done, start running
], function(err, data){
	if (err)
		logger.error(err);
	else //Run
		run(function(err, data){
			//Run finished, disconnect
			//logger.info('REDIS', 'Disconnect..');
			//redis.disconnect(function(){});
			logger.info('MONGODB', 'Disconnect..');
			mongodb.disconnect(function(){});
		});
});


/**
 * Run
 */
function run(cb){

	console.log('Running...');

 // var MatchSchema = new Schema({

 //  player1: { type: String, required: true },
 //  player2: { type: String, required: true },
 //  league: { type: String },
 //  date: { type: Date, required: true },
 //  sport: { type: String, required: true },
 //  odds: [{ 
 //    bookmaker: { type: String, unique: true }
 //    odd1: { type: Number },
 //    odd_tie: { type: Number },
 //    odd2: { type: Number }
 //  }],
 //  searcheable: { type: String, required: true }

 	var match = new Match({ 
 		sport: 'soccer',
      	player1: 'Celta',
      	player2: 'Vigo',
      date: '2017-06-02T19:00:00.000Z',
      league: '★\nSegunda División',
      odds: {
      	bookmaker: 'sportium',
      	odd1: '3.10',
     	oddt: '3.10',
      	odd2: '2.60' 
      },
      searcheable: 'celta vigo'
	});

 	var matchc = { 
 		sport: 'soccer',
      	player1: 'Celta',
      	player2: 'Vigo',
    	date: '2017-06-02T19:00:00.000Z',
    	league: '★ Segunda División',
    	odds: {
    		bookmaker: 'bwin',
	      	odd1: '40',
	     	oddt: '1.10',
	      	odd2: '2.60',
    	},
    	searcheable: 'celta vigo'
	};

	


	//logger.info(match);

	// match.save(function(err, data){
	// 	if (err)
	// 		logger.error(err);
	// 	else{
	// 		cb(null)
	// 		logger.debug('Done!');
	// 	}
	// })

	Match.searchMatch(matchc, function(err, data){
		if (err)
			logger.error(err);
		else{
			if (!data)
				logger.info('Not found...');
			else{
				logger.info('Found!', data);
				data.upsertOdds(matchc, function(err, data){
					if (err)
						logger.error(err);
					else
						logger.info('Finished!', data);

					cb(null);
				});
			}
		}
	});


	// Product.searchSimilar(product, function(err, products_similar){
	// 	if(err)
	// 		logger.error(err);
	// 	else{
	// 		logger.debug('Done');
	// 	}
	// 	cb(null);
	// });

}