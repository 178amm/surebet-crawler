'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

 /**
 * Bootstrap Mongoose models
 */


/**
 * Module dependencies
 */
var path = require('path');
var apppath = require('app-module-path').addPath(path.resolve(__dirname, '../helpers'));

const mongodb = require('../helpers/mongodb-connection');
	
const logger = require('logger');
const async = require('async');
const mongoose = require('mongoose');

const config = require('../config');
const Telegram = require('../helpers/Telegram');

require ('../app/models/Surebet');

var Surebet = mongoose.model('Surebet');


/**
 * DDBB Connect
 */
async.parallel([
	//Connect to the REDIS server
	//function(cb){redis.connect(null, cb)},
    //Connect to the MONGODB server
    function(cb){mongodb.connect(null, cb)},
//Once done, start running
], function(err, data){
	if (err)
		logger.error(err);
	else //Run
		run(function(err, data){
			//Run finished, disconnect
			//logger.info('REDIS', 'Disconnect..');
			//redis.disconnect(function(){});
			logger.info('MONGODB', 'Disconnect..');
			mongodb.disconnect(function(){});
		});
});


/**
 * Run
 */
function run(cb){

	console.log('Running...');

	var t = new Telegram(config.telegram);

	var surebetdata = { 
	    "match_data" : {
	        "odds" : [
	            {
	                "odd2" : 1.18, 
	                "odd1" : 2.13, 
	                "bookmaker" : "betfair"
	            }, 
	            {
	                "odd2" : 1.18, 
	                "odd1" : 1.45, 
	                "bookmaker" : "williamhill"
	            }
	        ], 
	        "surebet_coef" : -2.2134234234, 
	        "score" : 28.5, 
	        "searcheable" : "ewe baskets oldenburg brose baskets", 
	        "league" : "Copa BBL Alemana", 
	        "player2" : "Brose Baskets", 
	        "player1" : "EWE Baskets Oldenburg", 
	        "sport" : "basket", 
	    }
	};

	var surebet = new Surebet(surebetdata);
	surebet.save(function(err, data){
		if (err)
			logger.error(err);
		else{
			logger.info(surebet.toHTML());

			t.broadcast(surebet.toHTML(), function(err, data){
				if (err)
					logger.error(err);
				else
					logger.info(data);

				cb(err, data);
			});
		}
	});
}