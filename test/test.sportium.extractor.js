'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

 /**
 * Bootstrap Mongoose models
 */


/**
 * Module dependencies
 */
var path = require('path');
var apppath = require('app-module-path').addPath(path.resolve(__dirname, '../helpers'));

//const redis = require('../helpers/redis-singleton-connection');
const mongodb = require('../helpers/mongodb-connection');

const logger = require('logger');
const async = require('async');



/**
 * DDBB Connect
 */
async.parallel([
	//Connect to the REDIS server
	//function(cb){redis.connect(null, cb)},
    //Connect to the MONGODB server
    function(cb){mongodb.connect(null, cb)},
//Once done, start running
], function(err, data){
	if (err)
		logger.error(err);
	else //Run
		run(function(err, data){
			//Run finished, disconnect
			//logger.info('REDIS', 'Disconnect..');
			//redis.disconnect(function(){});
			logger.info('MONGODB', 'Disconnect..');
			mongodb.disconnect(function(){});
		});
});


/**
 * Run
 */
function run(cb){

	console.log('Running...');
	var url = 'http://sports.sportium.es/es/type-coupon?sb_type_ids=40796-41052';
	var parser_name = 'sportium_alt';
	var Extractor = require('../app/extractors/'+parser_name.capitalize());
	var ex = new Extractor();
	//var url = 'http://www.sephora.es/Maquillaje/Rostro/Blush/Spectrum-Palette-Blush-Paleta-de-coloretes/P2775044';
	//var parser_name = 'sephora';

	// Start script
	ex.getOddCrawlsFromURL(url, 'basket', function(err, data){
		if (err){
			logger.error(err);
			cb(err);
		}
		else{
			logger.info('Finished', data);
			logger.info();
			cb(null, data);
		}
	});
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}