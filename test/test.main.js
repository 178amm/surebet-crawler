'use strict';

/*
 * sofimakeup-crawler
 * Copyright(c) 2017 Adrian Martinez <adriandev.me@gmail.com>
 * MIT Licensed
 */

 /**
 * Bootstrap Mongoose models
 */


/**
 * Module dependencies
 */
var path = require('path');
var apppath = require('app-module-path').addPath(path.resolve(__dirname, '../helpers'));
require ('../app/models/Match');
require ('../app/models/Surebet');
//require ('../app/models/Notification');
//var mainProducts = require('../app/main.products.js');

//const redis = require('../node_modules_custom/redis-singleton-connection');
const mongodb = require('../helpers/mongodb-connection');
	
const logger = require('logger');
const async = require('async');
const mongoose = require('mongoose');

var Match = mongoose.model('Match');
var Surebet = mongoose.model('Surebet');


const main = require('../app/main');


/**
 * DDBB Connect
 */
async.parallel([
	//Connect to the REDIS server
	//function(cb){redis.connect(null, cb)},
    //Connect to the MONGODB server
    function(cb){mongodb.connect(null, cb)},
//Once done, start running
], function(err, data){
	if (err)
		logger.error(err);
	else //Run
		run(function(err, data){
			//Run finished, disconnect
			//logger.info('REDIS', 'Disconnect..');
			//redis.disconnect(function(){});
			logger.info('MONGODB', 'Disconnect..');
			mongodb.disconnect(function(){});
		});
});


/**
 * Run
 */
function run(cb){

	console.log('Running...');

 // var MatchSchema = new Schema({

 //  player1: { type: String, required: true },
 //  player2: { type: String, required: true },
 //  league: { type: String },
 //  date: { type: Date, required: true },
 //  sport: { type: String, required: true },
 //  odds: [{ 
 //    bookmaker: { type: String, unique: true }
 //    odd1: { type: Number },
 //    odd_tie: { type: Number },
 //    odd2: { type: Number }
 //  }],
 //  searcheable: { type: String, required: true }

 // 	var URLData = {
	// 	"extractor": "sportium_alt",
	// 	"sport": "basket",
	// 	"url": "http://sports.sportium.es/es/t/20696/Turkish-Basketball-League"
	// }

	var URLData = {		
		"extractor": "marathonbet_alt",
		"sport": "basket",
		"url": "https://www.marathonbet.es/es/popular/Basketball/?menu=6"
	}

	// var URLData = {		
	// 	"extractor": "williamhill",
	// 	"sport": "basket",
	// 	"url": "http://sports.williamhill.es/bet_esp/es/betting/y/3/mh/Baloncesto.html"
	// }

	// var URLData = {		
	// 	"extractor": "betfair_alt",
	// 	"sport": "basket",
	// 	"url": "https://www.betfair.es/sport/basketball?action=loadCompetition&modules=multipickavbId@1059&selectedTabType=FEATURED&couponListType=BY_COMPETITION"
	// }

	//logger.info(match);

	// match.save(function(err, data){
	// 	if (err)
	// 		logger.error(err);
	// 	else{
	// 		cb(null)
	// 		logger.debug('Done!');
	// 	}
	// })

	main.crawlOddsFromURLData(URLData, function(err, data){
		if (err)
			logger.error(err);
		else{
			logger.info('Finished!');
		}

		cb(err, data);
	});

}